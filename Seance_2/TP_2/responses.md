# Question 1

---
L'environnement peut être défini comme :
 - Accessible : Les agents ont une information complète et parfait sur 
l'environnement, ils ont d'informations sur les planètes et sur les autres vaisseaux, cela permettre les manageurs de 
déterminer les vaisseaux auxquels envoyer des messages et les vaisseaux de calculer la meilleure route ver une 
planète.
 - Déterministe : L'action de chaque vaisseau et chaque manageur a un effet déterministe sur le space, qui est
de créer un item et le délivrer.
 - Non épisodique : Par exemple, si un vaisseau a envoyé une proposition à un manageur, il ne pourra pas accepter
des autres appels pendant que le manageur traite sa proposition.
 - Statique : Il n'y a pas des changements sur l'environnement qui ne peuvent pas être
attribués aux agents.
 - Continu : Le nombre d'états du système ne peut pas être dénombrable, chaque état définit le nombre d'items disponibles
sur chaque planète et la position de chaque vaisseau (qui est continu) de sorte qu'il existe un nombre infinite d'états.

# Question 2

---
La classe `Item` fait partie de l'environnement, une partie des interactions des agents avec l'environnement 
est faite avec l'aide de ces éléments.

# Question 3

---
![](images/ex3_default.png)
La simulation avec les paramètres par default a généré l'image precedent, on peut voir que le nombre de biens délivrés
croît linéairement, pendant que le nombre d'items présents tend à plafonner et croire très légèrement.

En augmentant le nombre de planètes dans le système, le nombre d'items aura une croissance plus significative, vue 
que les vaisseaux ne seront pas tous suffisants pour délivrer tous les items.
![](images/ex3_plus_planetes.png)

Si on augmente le nombre de vaisseaux, le nombre d'items dans le système restera baisse vue qu'il aura presque toujours
un vaisseau dans la planète (si le nombre de vaisseaux est plus grande que celui de planètes).
![](images/ex3_plus_vaisseaux.png)

# Question 4

---
C'est une coopération, les agents échangent des messages pour trouver la meilleure façon d'envoyer un objet à un autre
planète.

# Question 5

---
Après les dernières modifications, le système devient dynamique, vue que les routes peuvent changer sans action 
des agents. Toutes les autres caractéristiques restes les mêmes.

# Question 6

---
Quand une route est bloquée, tous les vaisseaux qui utiliseront cette route, soient ils dans la route ou dans un des deux
planètes qui font la route, sont bloquées jusqu'à la route soit disponible. La caractéristique qui correspond à ce comportement
est l'adaptabilité, les agents sont capables d'interagir avec l'environnement et lorsque la route est disponible,
ils sont capables de retourner à leur activité.

# Question 7

---
![](images/ex_7.png)
Désormais, la quantité d'items délivrés est plus petite qu'avant, en significant que le changement des routes
a un impact significatif dans le système, qui rendre plus difficile aux vaisseaux d'agir dans l'environnement pour 
accomplir leurs objectifs. 

# Question 8

---
![](images/ex8_branch_08.png)
Dans l'image antérieure, avec un facteur de 0.8, on peut noter que l'augmentation du nombre de routes encadre une amelioration
dans le système, les agents sont capables de délivrer plus et contourner les problèmes créés par des routes bloqués.

# Considerations
Le code avait un problème avec jQuery qui n'était pas importé dans l'HTML et par consequent les 
items graphiques ne s'affichaient pas, j'ai ajouté le chemin de jquery dans la liste `local_includes`.

J'ai changé aussi la composition de thread de messages en modifient l'affichage d'item par l'item `uid`, 
afficher l'item dans le message n'apporte pas d'information aux agents et il me semble mieux d'envoyer l'`uid`.

J'ai aussi l'impression qu'il faut traiter les changements de routes dans les vaisseaux, actuellement les vaisseaux 
vont calculer la meilleure route ne s'important pas si elle est bloquée. Comme je n'étais pas sûre de comme faire 
cela, vue qu'éliminer l'arête du graphe peut créer des problèmes, j'ai préféré ne modifier pas les vaisseaux et man ternir
avec le comportement actuel, qui est d'être bloquée même s'il existe un autre chemin possible plus distant.