import enum
import math
import random
import uuid
from enum import Enum

import mesa
import numpy as np
from collections import defaultdict

import mesa.space
from mesa import Agent, Model
from mesa.datacollection import DataCollector
from mesa.time import RandomActivation
from mesa.visualization.ModularVisualization import VisualizationElement, ModularServer, UserSettableParameter
from mesa.visualization.modules import ChartModule

MAX_ITERATION = 100
PROBA_CHGT_ANGLE = 0.01


def move(x, y, speed, angle):
    return x + speed * math.cos(angle), y + speed * math.sin(angle)


def go_to(x, y, speed, dest_x, dest_y):
    if np.linalg.norm((x - dest_x, y - dest_y)) < speed:
        return (dest_x, dest_y), 2 * math.pi * random.random()
    else:
        angle = math.acos((dest_x - x)/np.linalg.norm((x - dest_x, y - dest_y)))
        if dest_y < y:
            angle = - angle
        return move(x, y, speed, angle), angle


class MarkerPurpose(Enum):
    DANGER = enum.auto(),
    INDICATION = enum.auto()


class ContinuousCanvas(VisualizationElement):
    local_includes = [
        "./js/simple_continuous_canvas.js",
        "./js/jquery.js"
    ]

    def __init__(self, canvas_height=500,
                 canvas_width=500, instantiate=True):
        VisualizationElement.__init__(self)
        self.canvas_height = canvas_height
        self.canvas_width = canvas_width
        self.identifier = "space-canvas"
        if (instantiate):
            new_element = ("new Simple_Continuous_Module({}, {},'{}')".
                           format(self.canvas_width, self.canvas_height, self.identifier))
            self.js_code = "elements.push(" + new_element + ");"

    def portrayal_method(self, obj):
        return obj.portrayal_method()

    def render(self, model):
        representation = defaultdict(list)
        for obj in model.schedule.agents:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
            representation[portrayal["Layer"]].append(portrayal)
        for obj in model.mines:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
            representation[portrayal["Layer"]].append(portrayal)
        for obj in model.markers:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
            representation[portrayal["Layer"]].append(portrayal)
        for obj in model.obstacles:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
            representation[portrayal["Layer"]].append(portrayal)
        for obj in model.quicksands:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
            representation[portrayal["Layer"]].append(portrayal)
        return representation


class Obstacle:  # Environnement: obstacle infranchissable
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def portrayal_method(self):
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 1,
                     "Color": "black",
                     "r": self.r}
        return portrayal


class Quicksand:  # Environnement: ralentissement
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def portrayal_method(self):
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 1,
                     "Color": "olive",
                     "r": self.r}
        return portrayal


class Mine:  # Environnement: élément à ramasser
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def portrayal_method(self):
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 2,
                     "Color": "blue",
                     "r": 2}
        return portrayal


class Marker:  # La classe pour les balises
    def __init__(self, x, y, purpose, direction=None):
        self.x = x
        self.y = y
        self.purpose = purpose
        if purpose == MarkerPurpose.INDICATION:
            if direction is not None:
                self.direction = direction
            else:
                raise ValueError("Direction should not be none for indication marker")

    def portrayal_method(self):
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 2,
                     "Color": "red" if self.purpose == MarkerPurpose.DANGER else "green",
                     "r": 2}
        return portrayal

def distance(x1, y1, x2, y2):
    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
class Robot(Agent):  # La classe des agents
    def __init__(self, unique_id: int, model: Model, x, y, speed, sight_distance, angle=0.0):
        super().__init__(unique_id, model)
        self.x = x
        self.y = y
        self.speed = speed
        self.sight_distance = sight_distance
        self.angle = angle
        self.counter = 0
        self.in_quicksand = False

    def find_robots_collision(self, angle):
        robots_near = []
        for robot in self.model.schedule.agents:
            if robot.unique_id != self.unique_id:
                # Calculate the nearest point in the line
                distance_vector = (robot.x - self.x, robot.y - self.y)
                speed_vector = (math.cos(angle) * self.speed, math.sin(angle) * self.speed)
                coeff_projection = (distance_vector[0] * speed_vector[0] + distance_vector[1] * speed_vector[1]) / self.speed ** 2
                projection_on_line = (speed_vector[0] * coeff_projection, speed_vector[1] * coeff_projection)
                closest_point = (self.x + projection_on_line[0], self.y + projection_on_line[1])

                # Check if the closes point is in between the robot and his next position
                if (closest_point[0] < self.x and closest_point[0] < self.x + speed_vector[0]) or \
                        (closest_point[0] > self.x and closest_point[0] > self.x + speed_vector[0]):
                    # It is not, then the closest point in this position is either the actual position
                    # of the robot or its next one
                    if distance(self.x, self.y, robot.x, robot.y) < distance(self.x + speed_vector[0],
                                                                             self.y + speed_vector[1],
                                                                             robot.x, robot.y):
                        closest_point = (self.x, self.y)
                    else:
                        closest_point = (self.x + speed_vector[0], self.y + speed_vector[1])

                if distance(closest_point[0], closest_point[1], robot.x, robot.y) < robot.speed:
                    robots_near.append(robot)

        return robots_near

    def find_obstacles_collision(self, angle):
        obstacles_near = []
        speed_vector = (math.cos(angle)*self.speed, math.sin(angle)*self.speed)
        new_position = (self.x + speed_vector[0], self.y+speed_vector[1])
        for obstacle in self.model.obstacles:
            if distance(new_position[0], new_position[1], obstacle.x, obstacle.y) < obstacle.r:
                obstacles_near.append(obstacle)
        return obstacles_near

    def get_nearest_mine(self):
        nearest_mine = None
        min_dist = float('+inf')
        for mine in self.model.mines:
            dist = distance(self.x, self.y, mine.x, mine.y)
            if dist < self.sight_distance:
                if min_dist > dist:
                    nearest_mine = mine
                    min_dist = dist
        return nearest_mine

    def get_nearest_marker(self):
        nearest_marker = None
        min_dist = float('+inf')
        for marker in self.model.markers:
            dist = distance(self.x, self.y, marker.x, marker.y)
            if dist < self.sight_distance:
                if min_dist > dist:
                    nearest_marker = marker
                    min_dist = dist
        return nearest_marker

    def step(self):
        #pass  # TODO L'intégralité du code du TP peut être ajoutée ici.

        # Vérifie si est dans la sable mouvant
        speed = self.speed
        in_quicksand = False
        for sable in self.model.quicksands:
            if distance(self.x, self.y, sable.x, sable.y) < sable.r:
                speed = self.speed/2
                in_quicksand = True
                self.in_quicksand = True
                self.model.time_in_quicksand += 1

        if self.in_quicksand and not in_quicksand:
            self.in_quicksand = False
            marker = Marker(self.x, self.y, MarkerPurpose.DANGER)
            self.model.markers.append(marker)
            self.counter = self.speed//2 + 1

        # Check for collisions
        speed_vector = (math.cos(self.angle) * self.speed, math.sin(self.angle) * self.speed)
        new_position = (self.x + speed_vector[0], self.y + speed_vector[1])
        if len(self.find_robots_collision(self.angle)) > 0 or len(self.find_obstacles_collision(self.angle)) > 0 or \
            self.model.space.out_of_bounds(new_position):
            # Find new angle
            for angle_add in random.sample(range(5, 360, 5), k= 355 // 5):
                can_collide = False
                new_angle = self.angle + angle_add * math.pi / 180
                new_speed_vector = (math.cos(new_angle)*self.speed, math.sin(new_angle)*self.speed)
                new_position = (self.x + new_speed_vector[0], self.y + new_speed_vector[1])
                if len(self.find_robots_collision(new_angle)) > 0:
                    can_collide = True
                if len(self.find_obstacles_collision(new_angle)) > 0:
                    can_collide = True
                if self.model.space.out_of_bounds(new_position):
                    can_collide = True

                if not can_collide:
                    break
            self.angle = self.angle + angle_add*math.pi/180
        elif self.get_nearest_mine() is not None:
            mine = self.get_nearest_mine()
            dist_vector = (mine.x - self.x, mine.y - self.y)
            dist = distance(self.x, self.y, mine.x, mine.y)
            if mine.x != self.x or mine.y != self.y:
                new_angle = math.atan2(dist_vector[1], dist_vector[0])
                self.angle = new_angle
                speed = dist if dist < speed else speed
            elif mine.x == self.x and mine.y == self.y:
                self.model.mines.remove(mine)
                marker = Marker(self.x, self.y, MarkerPurpose.INDICATION, self.angle)
                self.model.markers.append(marker)
                self.counter = self.speed // 2 + 1
        elif self.get_nearest_marker() is not None and self.counter <= 0:
            marker = self.get_nearest_marker()
            if marker.purpose == MarkerPurpose.DANGER:
                self.angle += math.pi
            else:
                dist_vector = (marker.x - self.x, marker.y - self.y)
                dist = distance(self.x, self.y, marker.x, marker.y)
                if marker.x != self.x or marker.y != self.y:
                    new_angle = math.atan2(dist_vector[1], dist_vector[0])
                    self.angle = new_angle
                    speed = dist if dist < speed else speed
                elif marker.x == self.x and marker.y == self.y:
                    self.model.markers.remove(marker)
                    self.angle = marker.direction + math.pi/2
        else:
            # Change la direction aléatoirement
            self.angle = random.random() * 2 * math.pi if random.random() <= PROBA_CHGT_ANGLE else self.angle
        # Se déplace
        self.x = self.x + speed*math.cos(self.angle)
        self.y = self.y + speed*math.sin(self.angle)
        self.counter -= 1

    def portrayal_method(self):
        portrayal = {"Shape": "arrowHead", "s": 1, "Filled": "true", "Color": "Red", "Layer": 3, 'x': self.x,
                     'y': self.y, "angle": self.angle}
        return portrayal


class MinedZone(Model):
    collector = DataCollector(
        model_reporters={"Mines": lambda model: len(model.mines),
                         "Danger markers": lambda model: len([m for m in model.markers if
                                                          m.purpose == MarkerPurpose.DANGER]),
                         "Indication markers": lambda model: len([m for m in model.markers if
                                                          m.purpose == MarkerPurpose.INDICATION]),
                         "Time in Quicksand": lambda model: model.time_in_quicksand},
        agent_reporters={})

    def __init__(self, n_robots, n_obstacles, n_quicksand, n_mines, speed):
        Model.__init__(self)
        self.space = mesa.space.ContinuousSpace(600, 600, False)
        self.schedule = RandomActivation(self)
        self.mines = []  # Access list of mines from robot through self.model.mines
        self.markers = []  # Access list of markers from robot through self.model.markers (both read and write)
        self.obstacles = []  # Access list of obstacles from robot through self.model.obstacles
        self.quicksands = []  # Access list of quicksands from robot through self.model.quicksands
        self.time_in_quicksand = 0
        for _ in range(n_obstacles):
            self.obstacles.append(Obstacle(random.random() * 500, random.random() * 500, 10 + 20 * random.random()))
        for _ in range(n_quicksand):
            self.quicksands.append(Quicksand(random.random() * 500, random.random() * 500, 10 + 20 * random.random()))
        for _ in range(n_robots):
            x, y = random.random() * 500, random.random() * 500
            while [o for o in self.obstacles if np.linalg.norm((o.x - x, o.y - y)) < o.r] or \
                    [o for o in self.quicksands if np.linalg.norm((o.x - x, o.y - y)) < o.r]:
                x, y = random.random() * 500, random.random() * 500
            self.schedule.add(
                Robot(int(uuid.uuid1()), self, x, y, speed,
                      2 * speed, random.random() * 2 * math.pi))
        for _ in range(n_mines):
            x, y = random.random() * 500, random.random() * 500
            while [o for o in self.obstacles if np.linalg.norm((o.x - x, o.y - y)) < o.r] or \
                    [o for o in self.quicksands if np.linalg.norm((o.x - x, o.y - y)) < o.r]:
                x, y = random.random() * 500, random.random() * 500
            self.mines.append(Mine(x, y))
        self.datacollector = self.collector

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()
        if not self.mines:
            self.running = False


def run_single_server():
    chart = ChartModule([{"Label": "Mines",
                          "Color": "Orange"},
                         {"Label": "Danger markers",
                          "Color": "Red"},
                         {"Label": "Indication markers",
                          "Color": "Green"},
                         {"Label": "Time in Quicksand",
                          "Color": "Blue"}
                         ],
                        data_collector_name='datacollector')
    server = ModularServer(MinedZone,
                           [ContinuousCanvas(),
                            chart],
                           "Deminer robots",
                           {"n_robots": UserSettableParameter('slider', "Number of robots", 7, 3,
                                                             15, 1),
                            "n_obstacles": UserSettableParameter('slider', "Number of obstacles", 5, 2, 10, 1),
                            "n_quicksand": UserSettableParameter('slider', "Number of quicksand", 5, 2, 10, 1),
                            "speed": UserSettableParameter('slider', "Robot speed", 15, 5, 40, 5),
                            "n_mines": UserSettableParameter('slider', "Number of mines", 15, 5, 30, 1)})
    server.port = 8521
    server.launch()


if __name__ == "__main__":
    run_single_server()
