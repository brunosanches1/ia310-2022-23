## Question 1
L'architecture réactive de subsomption est la plus appropriée à ce problème,
on pourra définir des comportements et leurs priorités selon le problème.

## Question 2
Priorités, du plus haut au plus bas :

- Éviter collisions (avec d'autres robots, l'environnement et des obstacles);
- Détruire une mine proche ;
- Se diriger vers une mine ;
- Se déplacer.

Temps moyen de désamorçage de toutes les mines : 250 steps

## Question 3
Tous les concepts d'agents réactifs sont respectés :

 - Incarnation : Les agents ont un corps et sont couplés au environnement, 
   ils le peuvent sentir à partir de leurs senseurs.
 - Situation : Le monde a une complexité qui est modélisée par les comportements et réactions des robots
 - Intelligence : Les robots ont une perception de space et des items à leur alentour.
 - Emergence : Il y a des interactions entre agents, qui se sont fait repellers par les autres,
    et aussi avec l'environnement descrits par les mines, les obstacles et les sables.
   
## Question 4
Les dernières modifications ajoutent un état interne aux robots, chacun doit
savoir s'il était sur la sable au step precedent et s'ils ont placé une 
balise, ces modifications sont contraires à la definition de l'architecture de subsomption.

## Question 5
En utilisant la même architecture, on peut reorganiser les comportements de la manière suivante, de la 
priorité plus haute jusqu'à plus bas :

 - Éviter collisions ;
 - Détruire une mine proche et placer une balise;
 - Se diriger ver uns miche proche ;
 - Vérifier les balises proches, se diriger ver une INDICATION et tourner si DANGER ;
 - Se déplacer et mettre une balise si sors de la sable.

## Question 6
![](images/img_ex6.png)
Temps moyen : 260 steps
On note que le temps moyen pour désarmorçer toutes les mines a crû, ça peut être expliqué
par des fois que quelques robots se sont fait isoler dans les sables par des balises
de danger qui les faits tourner sans arrêter et sans sortir de la sable, 
ça fait moins de robots utiles e plus de temps pour que les autres trouvent les mines.

## Question 7
Sans balises de danger:
![](images/img_ex7_2.png)

Avec balises de danger:
![](images/img_ex7_1.png)

On note que le nombre de pas où un robot est dans le sables est plus petit
quand il y a des balises indiquant le danger.

## Question bonus

